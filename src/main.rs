// spftrace – utility for tracing SPF queries
// Copyright © 2022–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

mod args;
mod config;
mod model;
mod parse;
mod print;
mod resolver;
mod result;
mod run;
mod trace;

#[cfg(test)]
mod tests;

use crate::{args::Args, run::RunError};
use std::{
    io::{stderr, Write},
    process,
};

const PROGRAM_NAME: &str = env!("CARGO_BIN_NAME");

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let args = match Args::parse() {
        Ok(args) => args,
        Err(e) => {
            let _ = writeln!(stderr(), "{PROGRAM_NAME}: {e}");
            process::exit(1);
        }
    };

    let Args { config, sender, ip } = args;

    if let Err(e) = run::run_trace(config, sender, ip, None).await {
        match e {
            RunError::Boxed(e) => {
                let _ = writeln!(stderr(), "{PROGRAM_NAME}: {e}");
                process::exit(1);
            }
            RunError::Internal(e) => {
                let _ = writeln!(stderr(), "{PROGRAM_NAME}: internal error: {e}");
                process::exit(2);
            }
        }
    }
}
