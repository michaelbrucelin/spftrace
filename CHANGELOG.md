# spftrace changelog

All notable user-facing changes are listed in this changelog.

This project follows Rust-flavoured [semantic versioning], with changes to the
minimum supported Rust version being considered breaking changes.

[semantic versioning]: https://doc.rust-lang.org/cargo/reference/semver.html

## 0.4.0 (unreleased)

The minimum supported Rust version is now 1.67.0.

### Changed

* The minimum supported Rust version has been raised to 1.67.0.

### Added

* The new option `-i`/`--initial-txt` allows substituting an artificial text
  record as the starting point of an SPF evaluation. This helps some exploration
  and experimentation uses.

## 0.3.0 (2023-09-20)

With this release, development has moved from GitLab to the [Codeberg] platform.
Other than that, this is a maintenance release with no notable functional
changes.

The minimum supported Rust version is now 1.65.0.

[Codeberg]: https://codeberg.org

### Changed

* The minimum supported Rust version has been raised to 1.65.0.

## 0.2.1 (2023-03-31)

### Added

* The short option `-s` has been added as an alias for `--system-resolver`.

### Fixed

* Fix a panic when printing an SPF record containing no terms at all.

## 0.2.0 (2023-02-15)

The minimum supported Rust version is now 1.61.0.

### Changed

* The minimum supported Rust version has been raised to 1.61.0.

### Fixed

* The program now terminates gracefully on I/O error when printing. Previously,
  it panicked when it could not write to standard output or standard error.

## 0.1.2 (2022-11-10)

### Changed

* Output colouring is now automatically disabled when a non-terminal output
  stream is detected or when the environment variable `NO_COLOR` is set.

## 0.1.1 (2022-08-15)

### Added

* The new option `--system-resolver` allows using the resolver configuration
  present on the system (for example, in `/etc/resolv.conf`) to configure the
  resolver used for SPF queries.
* The new option `-w`/`--line-width` can be used to control the maximum line
  width when printing wrapped items such as SPF records.

## 0.1.0 (2022-06-22)

Initial release.
